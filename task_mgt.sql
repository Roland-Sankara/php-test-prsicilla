-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2023 at 03:16 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_mgt`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_04_181927_create_roles_table', 1),
(6, '2024_10_12_000000_create_users_table', 1),
(7, '2024_10_14_180200_create_projects_table', 1),
(8, '2024_10_15_180231_create_milestones_table', 1),
(9, '2024_10_16_200720_create_project_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `milestones`
--

CREATE TABLE `milestones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `milestone_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `milestones`
--

INSERT INTO `milestones` (`id`, `project_id`, `milestone_name`, `description`, `status`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(6, 2, 'Maxine Buchanan', 'Inventore nisi eaque', 'completed', '2021-01-08 00:00:00', '2002-03-17 00:00:00', '2023-10-05 06:56:21', '2023-10-05 06:56:21'),
(7, 2, 'Forrest Robinson', 'Provident officia e', 'in-progress', '1978-09-28 00:00:00', '1993-07-17 00:00:00', '2023-10-05 06:56:21', '2023-10-05 06:56:21'),
(8, 1, 'Cameran Blackburn', 'Consequuntur fugiat', 'in-progress', '2016-07-26 00:00:00', '2006-12-25 00:00:00', '2023-10-05 07:11:14', '2023-10-05 07:11:14'),
(9, 1, 'Yoshio Meyers', 'Commodi deleniti mol', 'awaiting-start', '2022-09-01 00:00:00', '2010-07-17 00:00:00', '2023-10-05 07:11:14', '2023-10-05 07:11:14'),
(25, 3, 'Yvonne Shelton', 'Sed suscipit recusan', 'completed', '2004-11-21 00:00:00', '2006-09-12 00:00:00', '2023-10-05 07:19:04', '2023-10-05 07:19:04'),
(26, 3, 'Sacha Cline', 'Eius perspiciatis c', 'on-hold', '2021-11-28 00:00:00', '1994-04-21 00:00:00', '2023-10-05 07:19:04', '2023-10-05 07:19:04'),
(27, 3, 'Vivien Rogers', 'Laborum Ullam ut il', 'completed', '1981-02-07 00:00:00', '1983-08-05 00:00:00', '2023-10-05 07:19:04', '2023-10-05 07:19:04'),
(30, 4, 'Sophia Peters', 'Sit eius omnis elig', 'completed', '1996-11-02 00:00:00', '1973-11-15 00:00:00', '2023-10-05 07:28:45', '2023-10-05 07:28:45'),
(31, 4, 'Jackson Wilkinson', 'Laboriosam id saepe', 'completed', '1971-03-11 00:00:00', '2000-07-31 00:00:00', '2023-10-05 07:28:45', '2023-10-05 07:28:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Joshua Jacobs', 'Quia eos do non ani', 'In Progress', '2023-10-05 06:10:33', '2023-10-05 09:54:42'),
(2, 'Cathleen Fleming', 'Dolore quaerat velit', 'On Hold', '2023-10-05 06:54:54', '2023-10-05 09:54:45'),
(3, 'Marshall Elliott', 'Duis quaerat consequ', 'In Progress', '2023-10-05 07:11:43', '2023-10-05 09:03:17'),
(4, 'Roary Mason', 'Fugiat in provident', 'On Hold', '2023-10-05 07:28:11', '2023-10-05 10:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `project_user`
--

CREATE TABLE `project_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_user`
--

INSERT INTO `project_user` (`id`, `project_id`, `user_id`, `created_at`, `updated_at`, `role_id`) VALUES
(19, 3, 1, '2023-10-05 07:19:04', '2023-10-05 07:19:04', 1),
(24, 4, 13, '2023-10-05 07:28:11', '2023-10-05 07:28:11', 2),
(25, 4, 7, '2023-10-05 07:28:45', '2023-10-05 07:28:45', 1),
(26, 4, 10, '2023-10-05 07:28:45', '2023-10-05 07:28:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Developer', 'develops', NULL, NULL),
(2, 'Project Manager', 'manages', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Myra Dominguez', 'xibeqawemi@mailinator.com', NULL, '$2y$10$xipMqEdowTAL7DDWticmZOOYzBr85Toy75ztPpuezYPB297sGLpqK', NULL, '2023-10-04 17:11:23', '2023-10-04 17:11:23'),
(7, 1, 'Fulton Hall', 'fatapopyf@mailinator.com', NULL, '$2y$10$ZUzepzogkdo.IlrTEvt/ZO4pgm8YfaytgImnYCP4p3F2s9UM0Niea', NULL, '2023-10-05 07:20:17', '2023-10-05 07:20:17'),
(8, 1, 'Xander Mercado', 'muroqa@mailinator.com', NULL, '$2y$10$t3lgWHFmpt1pszZF2S4sh.XMa7kBrbLEaPPTv.160XcgnKN3miT/2', NULL, '2023-10-05 07:20:25', '2023-10-05 07:20:25'),
(9, 2, 'Jermaine Russell', 'xixizeduse@mailinator.com', NULL, '$2y$10$Y/p6EfE8NhaHw4XI76.KnOuC5gksMnKRHg2xGgWKzyfp9FbhOZ/s6', NULL, '2023-10-05 07:20:33', '2023-10-05 07:20:33'),
(10, 1, 'Prescott Trujillo', 'zyho@mailinator.com', NULL, '$2y$10$jDN3Hug6g9TV7L1JqcaF..keVLSFB8QKri1ZYbXvXFFkuBqiFIrN2', NULL, '2023-10-05 07:20:41', '2023-10-05 07:20:41'),
(11, 2, 'Keith Black', 'toro@mailinator.com', NULL, '$2y$10$OvfpQG0vqYCCLc4eeuKTNeMpgahb5oZK1NLuOi9ns5/f3mdBTlEJa', NULL, '2023-10-05 07:20:53', '2023-10-05 07:20:53'),
(12, 1, 'Harlan Mcmillan', 'fihusyzyp@mailinator.com', NULL, '$2y$10$K1WyxOVY4S5MLrQUPG.LS.KV8sJyLafBoyyb7/ipoHUqMYHQSgD2m', NULL, '2023-10-05 07:21:01', '2023-10-05 07:21:01'),
(13, 2, 'Fleur West', 'rohexuwyke@mailinator.com', NULL, '$2y$10$KPs0qwY2G0NvI9G55ezFhu8dpM.8k.2K5eI.86G4ZTJlUI0Ty9Dja', NULL, '2023-10-05 07:21:09', '2023-10-05 07:21:09'),
(14, 1, 'Keaton Lowe', 'xafyte@mailinator.com', NULL, '$2y$10$jmdxlApbSiZXh.RoLazaW..MwSVHLELOCScDA5TXPbGLNYPDTy15.', NULL, '2023-10-05 07:21:20', '2023-10-05 07:21:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `milestones`
--
ALTER TABLE `milestones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `milestones_project_id_foreign` (`project_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_user`
--
ALTER TABLE `project_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_user_project_id_foreign` (`project_id`),
  ADD KEY `project_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `milestones`
--
ALTER TABLE `milestones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project_user`
--
ALTER TABLE `project_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `milestones`
--
ALTER TABLE `milestones`
  ADD CONSTRAINT `milestones_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_user`
--
ALTER TABLE `project_user`
  ADD CONSTRAINT `project_user_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
